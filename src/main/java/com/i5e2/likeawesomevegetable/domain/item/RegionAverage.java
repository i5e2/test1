package com.i5e2.likeawesomevegetable.domain.item;

public interface RegionAverage {
    String getAuctionItem();

    Long getAuctionHighestPrice();

    Long getAuctionQuantity();
}

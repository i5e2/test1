package com.i5e2.likeawesomevegetable.domain.message;

public enum MessageChecked {
    MESSAGE_CHECKED,
    MESSAGE_UNCHECKED
}

package com.i5e2.likeawesomevegetable.domain.message;

public enum MessageType {
    SEND,
    GET
}

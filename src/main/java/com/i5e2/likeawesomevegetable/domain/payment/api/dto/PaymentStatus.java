package com.i5e2.likeawesomevegetable.domain.payment.api.dto;

import lombok.Getter;

@Getter
public enum PaymentStatus {
    READY, PAYMENT_SUCCESS, PAYMENT_FAIL
}

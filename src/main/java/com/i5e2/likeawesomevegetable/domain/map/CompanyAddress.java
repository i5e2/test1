package com.i5e2.likeawesomevegetable.domain.map;

public interface CompanyAddress {

    Long getId();
    String getCompanyName();
    String getCompanyAddress();
}
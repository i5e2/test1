package com.i5e2.likeawesomevegetable.domain.point.dto;

import lombok.Getter;

@Getter
public enum PointStatus {
    SAVE_POINT, USED_POINT, SPLIT_POINT
}

package com.i5e2.likeawesomevegetable.domain.deposit.dto;

import lombok.Getter;

@Getter
public enum DepositAvailableStatus {
    DEPOSIT_AVAILABLE, DEPOSIT_NOT_AVAILABLE
}
